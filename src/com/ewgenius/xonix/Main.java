package com.ewgenius.xonix;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: ewgenius
 * Date: 23.07.13
 * Time: 20:03
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] arguments) {
        try {
            AppGameContainer app;
            app = new AppGameContainer(new XonixGame("Amputator"));
            app.setShowFPS(false);
            app.setDisplayMode(800, 600, false);
            app.start();
        } catch (SlickException ex) {
            Logger.getLogger(XonixGame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
