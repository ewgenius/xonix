package com.ewgenius.xonix;

import com.ewgenius.xonix.levels.Level1;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Created with IntelliJ IDEA.
 * User: ewgenius
 * Date: 24.07.13
 * Time: 14:58
 * To change this template use File | Settings | File Templates.
 */
public class XonixGame extends StateBasedGame {
    public XonixGame(String gamename) {
        super(gamename);
    }

    public void initStatesList(GameContainer container) {
        addState(new Level1());
    }
}
