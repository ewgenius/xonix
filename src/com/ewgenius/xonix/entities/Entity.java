package com.ewgenius.xonix.entities;

import com.ewgenius.xonix.levels.Level;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Created with IntelliJ IDEA.
 * User: ewgenius
 * Date: 25.07.13
 * Time: 19:21
 * To change this template use File | Settings | File Templates.
 */
public class Entity {
    protected Vector2f position;
    protected Vector2f speed;
    protected Level currentLevel;

    public Entity() {
        position = new Vector2f();
        speed = new Vector2f();
    }

    public Vector2f getPosition() {
        return position;
    }

    public Vector2f getSpeed() {
        return speed;
    }

    public void update(GameContainer container, StateBasedGame game, int deltaTime) {
        currentLevel = (Level) game.getCurrentState();
    }

    public void render(GameContainer container, Graphics graphics) {
    }
}
