package com.ewgenius.xonix.entities;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Created with IntelliJ IDEA.
 * User: ewgenius
 * Date: 30.07.13
 * Time: 14:41
 * To change this template use File | Settings | File Templates.
 */
public class Field extends Entity {
    private int width = 70;
    private int height = 50;
    private float cellWidth = 10;
    private float cellHeight = 10;
    private Color backgroundColor = Color.black;
    private Color[] cellColor = {
            Color.darkGray,
            Color.lightGray,
            Color.red,
    };
    private int[][] bitmap;

    public Field() {
        position.set(50, 50);
        bitmap = new int[height][];
        for (int i = 0; i < height; i++) {
            bitmap[i] = new int[width];
            for (int j = 0; j < width; j++) {
                if (i == 0 || i == height - 1 || j == 0 || j == width - 1)
                    bitmap[i][j] = 1;
                else
                    bitmap[i][j] = 0;
            }
        }

    }

    public void render(GameContainer container, StateBasedGame game, Graphics graphics) {
        graphics.setColor(backgroundColor);
        graphics.fillRect(position.x, position.y, width * cellWidth, height * cellHeight);

        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++) {
                graphics.setColor(cellColor[bitmap[i][j]]);
                graphics.fillRect(position.x + j * cellWidth + 1, position.y + i * cellHeight + 1, cellWidth - 2, cellHeight - 2);
            }
    }

}
