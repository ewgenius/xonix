package com.ewgenius.xonix.gui;

import org.newdawn.slick.geom.Vector2f;

/**
 * Created with IntelliJ IDEA.
 * User: ewgenius
 * Date: 25.07.13
 * Time: 19:06
 * To change this template use File | Settings | File Templates.
 */
public abstract class GuiElement {
    protected Vector2f position;
    protected int width;
    protected int height;
}
