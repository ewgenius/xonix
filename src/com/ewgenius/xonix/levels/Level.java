package com.ewgenius.xonix.levels;

import com.ewgenius.xonix.entities.Player;
import com.ewgenius.xonix.entities.Field;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ewgenius
 * Date: 24.07.13
 * Time: 15:05
 * To change this template use File | Settings | File Templates.
 */
public class Level extends BasicGameState {
    protected int id;
    protected String name;
    protected Field field;
    protected Player player;

    public Level(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        field = new Field();
        player = new Player();
    }

    public void render(GameContainer container, StateBasedGame game, Graphics graphics) {
        field.render(container, game, graphics);
        player.render(container, graphics);
    }

    public void update(GameContainer container, StateBasedGame game, int deltaTime) {
        //ground.update(container, game, deltaTime);
        player.update(container, game, deltaTime);
    }

    public int getID() {
        return id;
    }
}
